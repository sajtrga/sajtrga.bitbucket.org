
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function klicGenerirajPodatke(){

	$('#obvestilaGenerirej').text("");
	generirajPodatke(1);
	generirajPodatke(2);
	generirajPodatke(3);
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";
	
	if(stPacienta == 1){
		ehrId = "7dad6fbf-8ae6-4414-aca8-0f81143e44be"
		var tezz = Math.round((Math.random() * 100) /2);
		var viss = Math.round((Math.random() * 100)/2+100);
		var siss = Math.round((Math.random() * 100) /2+60);
		var diss = Math.round((Math.random() * 100) /3+40);
		dodajPodatkeEHR(ehrId,viss,tezz,siss,diss);
		$('#obvestilaGenerirej').append("</br>Susedov Francl: Višina: "+viss+"cm, Teža: "+tezz+"kg, Tlak: "+siss+"/"+diss+"mmHg");
	}
	else if(stPacienta == 2){
		ehrId = "ef8e6d49-af85-49c6-ad49-2dd9a27161ba"
		var tezz = Math.round((Math.random() * 100) /2+80);
		var viss = Math.round((Math.random() * 100)/2+165);
		var siss = Math.round((Math.random() * 100) /2+110);
		var diss = Math.round((Math.random() * 100) /3+70);
		dodajPodatkeEHR(ehrId,viss,tezz,siss,diss);
		$('#obvestilaGenerirej').append("</br>Šuštarjeva Mici: Višina: "+viss+"cm, Teža: "+tezz+"kg, Tlak: "+siss+"/"+diss+"mmHg");
	}
	else if(stPacienta == 3){
		ehrId = "db3d3030-0a7f-4ad0-be50-71b9fcb19212"
		var tezz = Math.round((Math.random() * 100) /2+30);
		var viss = Math.round((Math.random() * 100)/2+120);
		var siss = Math.round((Math.random() * 100) /2+80);
		var diss = Math.round((Math.random() * 100) /3+65);
		dodajPodatkeEHR(ehrId,viss,tezz,siss,diss);
		$('#obvestilaGenerirej').append("</br>Guspod Dohtar: Višina: "+viss+"cm, Teža: "+tezz+"kg, Tlak: "+siss+"/"+diss+"mmHg");
	}

  return ehrId;
}

function dodejPodatke(){
	var vis = $('#visina').val();
	var tez = $('#teza').val();
	var sis = $('#sistol').val();
	var dis = $('#diastol').val();
	var idd = $('#EHRID').val();
	dodajPodatkeEHR(idd,vis,tez,sis,dis);
}

function dodajPodatkeEHR(ehrId, visina, teza, sistol, distol){
	var sessid = getSessionId();
	$.ajaxSetup({
	headers: {
		"Ehr-Session":sessid
	}});
	var data = {
		"ctx/language": "en",
		"ctx/territory": "SI",
		"ctx/time": "2016-01-01",
		"vital_signs/height_length/any_event/body_height_length": visina,
        	"vital_signs/body_weight/any_event/body_weight": teza,
        	"vital_signs/blood_pressure/any_event/systolic": sistol,
        	"vital_signs/blood_pressure/any_event/diastolic": distol
	};
	var parampampam = {
		ehrId: ehrId,
		templateId: 'Vital Signs',
		format: 'FLAT',
		committer: 'jst'
	};
	$.ajax({
		url: baseUrl + "/composition?" + $.param(parampampam),
		type: 'POST',
		contentType: 'application/json',
		data: JSON.stringify(data),
		success: function(odgovor){
			
	$('#obvestilaGenerirejj').text("Podatki dodani");
		},
		error: function(error){
	$('#obvestilaGenerirej').text("napaka");
			console.log("napaka"+error);
		}
	});
}

function dudejEHR(){
	dodejVBazo($('#dudejIme').val(),$('#dudejPriimek').val(),$('#dudejDatum').val());
}

function dodejVBazo(iime, ppriimek, ddatum){
	sessId = getSessionId();
	$.ajaxSetup({
	headers: {
		"Ehr-Session": sessId
	}});
	var odg = $.ajax({
		url: baseUrl + '/ehr',
		async: false,
		type: 'POST',
		success: function(data) {
			var ehrId = data.ehrId;
			var partyData = {
				firstNames: iime,
				lastNames: ppriimek,
				dateOfBirth: ddatum,
				partyAdditionalInfo: [{
					key: "ehrId", value: ehrId
				}]
		};
		$.ajax({
			url: baseUrl + "/demographics/party",
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(partyData),
			success: function(party){
				$('#obvestilaDudej').text('Vaš EHR ID: ' + ehrId);
			},
			error: function(error){
				$('#obvestilaDudej').text('napaka');
			}
		});
	}
	});
}

function bregej(){
sessionId = getSessionId();
	var	iddd = $('#EHRIDDD').val();
	console.log(iddd);
	$('#tolePobris').text("");
	if (!iddd || iddd.trim().length == 0) {
		$('#tolePobris').text("manjka ER-HR ID");
		return;
	}
	$.ajax({
            url: baseUrl + "/view/" + iddd + "/" + "blood_pressure?" + $.param({
                limit: 25
            }),
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
		success: function(tlk){
	$.ajax({
                    url: baseUrl + "/view/" + iddd + "/" + "weight",
                    type: 'GET',
                    headers: {
                        "Ehr-Session": sessionId
                    },
		success: function(tez){
	$.ajax({
                    url: baseUrl + "/view/" + iddd + "/" + "height",
                    type: 'GET',
                    headers: {
                        "Ehr-Session": sessionId
                    },	
		success: function(vis){
			prikaziPodatke(tlk,tez,vis);
		},
	error: function(){
		console.log("napaka");
	}
});
	},
	error: function(){
		console.log("napaka");
	}
});
	},
	error: function(){
		console.log("napaka");
	}
});
	
}

function prikaziPodatke(tlk,tez,vis) {
	var i = vis.length-1;
	var teza = tez[i].weight;
	var tmp = 0;
	var visina = vis[i].height;
	var sistolicni = tlk[i].systolic;
	var diastolicni = tlk[i].diastolic;
	var tezamesage = "";
	var visinamessage = "";
	var sistolmessage = "";
	var diastolmessage = "";
	console.log(teza,visina,sistolicni,diastolicni);
	
	if(teza < 65){
		tmp++;
		$('#tolePobris').append("<p>Vaša teža je PODPOVPREČNA</p>");
		tezamessage = "moja%20teza%20je%20PODPOVPRECNA,%20";
	}
	if(teza > 75){
		tmp++;
		$('#tolePobris').append("<p>Vaša teža je NADPOVPREČNA</p>");
		tezamessage = "moja%20teza%20je%20NADPOVPRECNA,%20";
	}
	if(visina < 170){
		tmp++;
		$('#tolePobris').append("<p>Vaša višina je PODPOVPREČNA</p>");
		visinamessage = "moja%20visina%20je%20PODPOVPRECNA,%20";
	}
	if(visina > 180){
		tmp++;
		$('#tolePobris').append("<p>Vaša teža je NADPOVPREČNA</p>");
		visinamessage = "moja%20visina%20je%20NADPOVPRECNA,%20";
	}
	if(sistolicni < 115){
		tmp++;
		$('#tolePobris').append("<p>Vaš sistolični tlak je PODPOVPREČEN</p>");
		sistolmessage = "moj%20sistolicni%20tlak%20je%20PODPOVPRECEN,%20";
	}
	if(sistolicni > 125){
		tmp++;
		$('#tolePobris').append("<p>Vaš sistolični tlak je NADPOVPREČEN</p>");
		sistolmessage = "moj%20sistolicni%20tlak%20je%20NADPOVPRECEN,%20";
	}
	if(diastolicni < 75){
		tmp++;
		$('#tolePobris').append("<p>Vaš diastolični tlak je PODPOVPREČEN</p>");
		diastolmessage = "moj%20diastolicni%20tlak%20je%20PODPOVPRECEN,%20";
	}
	if(diastolicni > 85){
		tmp++;
		$('#tolePobris').append("<p>Vaš diastolični tlak je NADPOVPREČEN</p>");
		diastolmessage = "moj%20diastolicni%20tlak%20je%20NADPOVPRECEN,%20";
	}
	if(tmp==0){
		$('#tolePobris').append("<p>Žal ste popolnoma povprečni!</p>");
		diastolmessage = "sem%20popolnoma%20POVPRECEN,%20";
	}
	narisiGraf(tmp);
	$('#tvitni').append("<p></p><h3 style='text-align:center;'>TVITNI:</h3><p style='text-align:center;'><a class='twitter-share-button' href='https://twitter.com/intent/tweet?text="+tezamessage+visinamessage+sistolmessage+diastolmessage+"%20%23BRAGGRR' data-size='large'><img src='https://static.addtoany.com/images/blog/tweet-button-2015.png' style='width:100px'></a></p>");
}

function narisiGraf(num){
$(function() {
	var data = [num];
        $('#tolePaDodej').highcharts({
		chart: {
			type: 'bar'
		},
            title: {
                text: 'Število izvenpovprečnih stvari o vas',
                x: -20 //center
            },
            xAxis: {
                categories: ['izvenpovprečnost',
                ]
            },
            yAxis: {
		max: 4,
		min: 0,
                title: {
                    text: 'število'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'izvenpovprečnost',
                data: data
            }]
        });
    });
}

$(window).load(function() {
    $("#vzorcnipacienti").change(function() {
        $('#EHRIDDD').val($('#vzorcnipacienti').val());
    });

});
